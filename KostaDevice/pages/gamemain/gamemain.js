//gamemain.js
//获取应用实例
var app = getApp()

function _currentPage (){
  const pages = getCurrentPages()
  if(pages && pages.length > 1) {
    return pages[pages.length - 1]
  } else {
    return null
  }
}

function _bcolor (txt) {
  const n = +txt
  if(n <= 2) {
    return 'white'
  } else {
    return 'black'
  }
}

function _color(txt) {
  const n = +txt
  if(n >= 0 && n <= 7) {
    switch(n) {
      case 1:
        return 'purple'
      case 2:
        return 'blue'
      case 3:
        return 'cyan'
      case 4:
        return 'green'
      case 5:
        return 'yellow'
      case 6:
        return 'orange'
      case 7:
        return 'red'
      default:
        return 'white'
    }
  }
  return 'white'
}

function _touchOffset(e) {
  const that = _currentPage(), dx = that.data['dxcount'];
  if(e.touches && e.touches.length > 0) {
    return {
      x : parseInt(e.touches[0].x / 34),
      y : parseInt(e.touches[0].y / 34),
      index: parseInt(parseInt(e.touches[0].y / 34) * dx + parseInt(e.touches[0].x / 34))
    }
  }
  return {x:-1,y:-1,index:-1}
}

function _initDataArray (dx, dy) {
  var data = [],x ,y;
  for(y = 0; y < dy; y = y + 1) {
      for(x = 0; x < dx; x = x + 1) {
          data[y * dx + x] = '1'
      }
  }
  return data;
}

function _drawPage(page) {
  const ctx = wx.createCanvasContext('main'), pd = page.data['pd'], swidth = page.data['swidth'], sheight = page.data['sheight'], sq = 32, fontsize = 10
    _drawState(ctx, pd, fontsize, swidth, sheight, sq);
}

function _drawState(ctx, data, fontSize, width, height, sq) {
    var x, y, txt, i = 0
    ctx.setFontSize(fontSize)
    for(y = 0; y < height; y = y + sq + 2) {
        for(x = 0; x < width; x = x + sq + 2) {
          txt = data[i]
          ctx.save()
          ctx.setFillStyle(_color(txt))
          ctx.fillRect(x, y, sq, sq)
          ctx.restore()
          ctx.setFillStyle(_bcolor(txt))
          ctx.fillText(txt || "", x + 12, y + sq / 2 + 4)
          ++i
        }
    }
    ctx.draw()
}

function _canvasTouchStart (e) {
  const that = _currentPage(), offset = _touchOffset(e), index = offset.index
  var pd
  if(that && index > 0){
    pd = that.data['pd']
    pd[index] = '' + (++pd[index])
    that.setData({
      pd: pd
    })
    _drawPage(that)
  }
  
  console.log(e)
}

function _canvasTouchMove (e) {
  console.log(e)
}

function _canvasTouchEnd (e) {
  console.log(e)
}

Page({
  data: {
    userInfo: {},
    lastx: 0,
    lasty: 0,
    swidth: 320,
    sheight: 548,
    pd: [],
    dxcount: 0,
    dycount: 0,
    gamemainstyle: 'width:306px; height:578px'
  },
  canvasTouchStart: _canvasTouchStart,
  canvasTouchMove: _canvasTouchMove,
  canvasTouchEnd: _canvasTouchEnd,
  onLoad: function () {
    var that = this
    wx.getSystemInfo({
      success: function(res) {
        var w, h, dx, dy
        dx = parseInt(res.screenWidth / 34)
        dy = parseInt(res.screenHeight / 34)
        w = dx * 34
        h = dy * 34
        var gms = 'width:' + w + 'px; height:' + h + 'px'
        that.setData({
          gamemainstyle: gms,
          swidth: res.screenWidth,
          sheight: res.screenHeight,
          dxcount: dx + 1,
          dycount: dy + 1,
          pd: _initDataArray(dx + 1, dy + 1)
        })
      }
    })
  },
  onReady: function (){
    _drawPage(this)
  }
})
